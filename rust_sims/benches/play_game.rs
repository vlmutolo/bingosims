use criterion::{criterion_group, criterion_main, Criterion};
use win_locations::Game;

fn play_game(n_boards: usize) {
    let _result = Game::new_random(1, n_boards).play();
}

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("play 1000", |b| b.iter(|| play_game(1000)));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
