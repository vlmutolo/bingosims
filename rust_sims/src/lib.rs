use bit_set::BitSet;
use rand::rngs::{EntropyRng, SmallRng};
use rand::seq::SliceRandom;
use rand::{FromEntropy, Rng};
use serde_derive::Serialize;

type ColType = u32;
const B_COLUMN: [ColType; 15] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
const I_COLUMN: [ColType; 15] = [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
const N_COLUMN: [ColType; 15] = [31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45];
const G_COLUMN: [ColType; 15] = [46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60];
const O_COLUMN: [ColType; 15] = [61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75];

const ALL_BALLS: [ColType; 75] = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
    27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
    51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
    75,
];

#[derive(Debug)]
pub struct Game {
    game_id: u32,
    picks: Picks,
    boards: Vec<Board>,
    results: Option<GameResult>,
}

impl Game {
    pub fn new_random(game_id: u32, num_boards: usize) -> Game {
        // We use a slow rng with large period to generate the picks,
        // and a fast rng with smaller period to generate the boards.
        let mut picks_rng = EntropyRng::new();
        let mut boards_rng = SmallRng::from_entropy();
        let picks = Picks::new_random(&mut picks_rng);
        let boards: Vec<Board> = (0..num_boards)
            .map(|id| Board::new_random(id as u32, &mut boards_rng))
            .collect();

        Game {
            game_id,
            picks,
            boards,
            results: None,
        }
    }

    pub fn play(&mut self) -> GameResult {
        let mut check_buffer = BitSet::with_capacity(12);

        let expected_num_wins = 100;
        let mut board_ids: Vec<u32> = Vec::with_capacity(expected_num_wins);
        let mut win_types: Vec<u8> = Vec::with_capacity(expected_num_wins);

        for _round_num in 0..75 {
            self.picks.take_next();

            for board in self.boards.iter() {
                board.check(&self.picks, &mut check_buffer);
                for win_type in check_buffer.iter() {
                    board_ids.push(board.board_id);
                    win_types.push(win_type as u8);
                }
                check_buffer.clear();
            }

            if !win_types.is_empty() {
                break;
            }
        }

        GameResult {
            board_ids,
            win_types,
            rounds_played: self.picks.pick_set.len() as u8,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct GameResult {
    board_ids: Vec<u32>,
    win_types: Vec<u8>,
    rounds_played: u8,
}

#[derive(Debug)]
pub struct Board {
    board_id: u32,
    pub sequences: Vec<BitSet<ColType>>,
}

impl Board {
    // The general strategy here is to generate 12 bit sets, each representing
    // a winning sequence, and then collect them into one `Board`.
    pub fn new_random<R: Rng + ?Sized>(board_id: u32, mut rng: &mut R) -> Board {
        // Choose 5 numbers from each column. These are lazy iterators.
        let board_col_iters = vec![
            B_COLUMN.choose_multiple(&mut rng, 5),
            I_COLUMN.choose_multiple(&mut rng, 5),
            N_COLUMN.choose_multiple(&mut rng, 5),
            G_COLUMN.choose_multiple(&mut rng, 5),
            O_COLUMN.choose_multiple(&mut rng, 5),
        ];

        Board::from_col_iters(board_id, board_col_iters)
    }

    fn from_col_iters<'a, I>(board_id: u32, col_iters: Vec<I>) -> Board
    where
        I: Iterator<Item = &'a u32>,
    {
        // Generate a list of bitsets to store the winning sequences.
        let mut sequences = vec![BitSet::with_capacity(5); 12];

        for (col_idx, col_iter) in col_iters.into_iter().enumerate() {
            for (row_idx, board_num) in col_iter.enumerate() {
                // Check into what vertical sequence we should insert.
                sequences[col_idx].insert(*board_num as usize);

                // Check into what horizontal sequence we should insert.
                sequences[5 + row_idx].insert(*board_num as usize);

                // Check into what (if any) diagonal sequences we should insert.
                if col_idx == row_idx {
                    sequences[10].insert(*board_num as usize);
                }
                if col_idx == (4 - row_idx) {
                    sequences[11].insert(*board_num as usize);
                }
            }
        }

        Board {
            board_id,
            sequences,
        }
    }

    fn check(&self, picks: &Picks, bit_set_buffer: &mut BitSet) {
        bit_set_buffer.clear();
        for (win_type, sequence) in self.sequences.iter().enumerate() {
            let is_win = sequence.intersection(&picks.pick_set).count() >= 5;
            if is_win {
                bit_set_buffer.insert(win_type);
            }
        }
    }
}

#[derive(Debug)]
struct Picks {
    picks_full: Vec<ColType>,
    pick_set: BitSet<ColType>,
}

impl Picks {
    fn new_random<R: Rng + ?Sized>(mut rng: &mut R) -> Picks {
        let mut picks_full = Vec::with_capacity(75);
        picks_full.extend_from_slice(&ALL_BALLS);
        picks_full.shuffle(&mut rng);

        Picks {
            picks_full,
            pick_set: BitSet::with_capacity(75),
        }
    }

    fn take_next(&mut self) {
        let new_pick_idx = self.pick_set.len() + 1;
        self.pick_set.insert(self.picks_full[new_pick_idx] as usize);
    }
}
