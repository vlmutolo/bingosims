use indicatif::{ProgressBar, ProgressStyle};
use rayon::prelude::*;
use serde_json;
use std::fs;
use win_locations::{Game, GameResult};

fn main() {
    let num_boards = 100_000;
    let num_games = 12 * 500;

    let pbar = ProgressBar::new(num_games);
    pbar.set_style(
        ProgressStyle::default_bar()
            .template("[{elapsed_precise}] {bar:40.cyan/blue} {pos:>7}/{len:7} {msg}, ETA: {eta}"),
    );

    let result_list: Vec<GameResult> = (0..num_games as u32)
        .into_par_iter()
        .map(|i| {
            pbar.inc(1);
            Game::new_random(i, num_boards).play()
        })
        .collect();

    let serialized_json = serde_json::to_string(&result_list).unwrap();
//    fs::write("../data_product/wins.json", serialized_json).expect("Couldn't make file.");
}
