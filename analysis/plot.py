import pandas as pd
import matplotlib.pyplot as plt

wins = pd.read_feather("../data_product/wins.feather")

# wins.hist(by="win_types")

wins_by_type = wins['win_types'].value_counts(sort=False)

wins_by_type.plot(kind='bar')

plt.show()