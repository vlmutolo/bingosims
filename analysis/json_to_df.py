import json
import pandas as pd

# Read in file from data_products folder.
with open("../data_product/wins.json", "r") as f:
	games_json = json.loads(f.read())

# Construct a dataframe out of the dictionary.
df_list = []
for (game_idx, game) in enumerate(games_json):
	df = pd.DataFrame(games_json[game_idx])
	df['game_id'] = game_idx
	df_list.append(df)

# Gather all of the dataframes into one.
df_full = pd.concat(df_list).reset_index(drop=True)

# Save the dataframe as a feather file.
df_full.to_feather("../data_product/wins.feather")